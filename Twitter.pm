package Twitter;
#
# Net::Twitter - OAuth desktop app example
#
use warnings;
use strict;
 
use Net::Twitter;
use File::Spec;
use Storable;
use Data::Dumper;

sub TwitterOAuth {
# You can replace the consumer tokens with your own;
# these tokens are for the Net::Twitter example app.
    my %consumer_tokens = (
        consumer_key    => '2RTC5awD8cA97w3Ta9eJgw',
        consumer_secret => 'h27CS6lWWPXAB5hUVHuxXFfAzIDLZcREfTeaLvT02eE',
    );

    my $nt = Net::Twitter->new(traits => [qw/API::RESTv1_1/], %consumer_tokens);
    my $auth_url = $nt->get_authorization_url;
    print "[+] Authorize this application at: $auth_url\n";

    my ($pin, $account) = TwitterUserAuthenticate($auth_url);

    my @access_tokens = $nt->request_access_token(verifier => $pin);
    return (\@access_tokens, $account);
}

sub TwitterUserAuthenticate {
    my $auth_url = shift or die $!;
#### username & password authtentication
    my $ua      = LWP::UserAgent->new(); 
#     my $response = $ua->post($url, $parameter);
    my $response = $ua->get($auth_url);
    my $content = $response->as_string();
# <input name="authenticity_token" type="hidden" value="83ec0eb25f9c168eae24ac774ba0013fbdea3648" />
    if($content =~ m/.* name="authenticity_token" type="hidden" value="([\w]*)"/is) {
        my $authenticity_token = $1;
        if($content =~ m/.* name="oauth_token" type="hidden" value="([\w]*)"/is) {
            my $oauth_token = $1;
            print "[+] OAuth Token = $oauth_token\n";
            print "[+] Authenticity Token = $authenticity_token\n";

#             authenticity_token=25d90f1a0c18601b517fe12c5d8db614d8215ccd&oauth_token=fSqo77sH5PMp8wMeHqr8JbISpv9FvqcibLpARpkxw&session%5Busername_or_email%5D=ggyy%40sharklasers.com&session%5Bpassword%5D=1qaz2wsx
            my ($account, $password) = getTwitterAccount();
            my $param = {
                authenticity_token => $authenticity_token,
                oauth_token => $oauth_token,
                "session[username_or_email]" => $account,
                "session[password]" => $password
            };
            $response = $ua->post("https://api.twitter.com/oauth/authorize", $param);
            $content = $response->as_string();
            if($content =~ m|code>(\d+)</code>|is) {
                my $pin = $1;
                print "[+] Pin = $pin\n";
                return ($pin, $account);
            }
            elsif($content =~ m|Invalid user name or password\.|is) {
                print "\033[1;31m[!] Login Failed. Invalid user name or password.\033[0m\n";
                exit;
            }
        }
    }
}

sub getTwitterAccount {
    print "\n\033[1;32mEnter your information,\033[0m \n";
    print "\033[1;33mTwitter's account:\033[0m ";
    my $account = <STDIN>;
    print "\033[1;33mTwitter's password: \033[0m";
    my $password = <STDIN>;
    chomp $account;
    chomp $password;
    print "\n";
#     print "[+] Account: $account\n";
#     print "[+] Password: $password\n";
    return ($account, $password);
}
 
# my $access_tokens = TwitterOAuth();
# print "[+] user_token = $access_tokens->[0]\n";
# print "[+] user_secret = $access_tokens->[1]\n";
# print "[+] userid = $access_tokens->[2]\n";
# print "[+] username = $access_tokens->[3]\n";
# 
1
