package Facebook;

use strict;
use LWP::UserAgent;
use HTTP::Cookies;
use JSON;

sub loginFacebook {
    my $email = shift or die $!;
    my $pass = shift or die $!;
    my $token = "";

    my $ua = LWP::UserAgent->new(); 
    $ua->agent('Dalvik/1.6.0 (Linux; U; Android 4.1.2; GT-I9300 Build/JZO54K)');
    $ua->cookie_jar(HTTP::Cookies->new());

    push @{ $ua->requests_redirectable }, 'POST';
    my $response = $ua->get('https://m.facebook.com/login.php?skip_api_login=1&api_key=412831125460795&signed_next=1&next=https%3A%2F%2Fm.facebook.com%2Fdialog%2Foauth%3Fredirect_uri%3Dfb412831125460795%253A%252F%252Fauthorize%26display%3Dtouch%26scope%26type%3Duser_agent%26client_id%3D412831125460795%26ret%3Dlogin%26sdk%3Dios&cancel_uri=fb412831125460795%3A%2F%2Fauthorize%2F%3Ferror%3Daccess_denied%26error_code%3D200%26error_description%3DPermissions%2Berror%26error_reason%3Duser_denied%23_%3D_&display=touch&_rdr');
    print $response->as_string();

    $response = $response->decoded_content();
    if($response =~ m/ name="lsd" value="([^"]*)" /is) {
        my $lsd = $1;
        if($response =~ m/ name="charset_test" value="([^"]*)" /is) {
            my $charset_test = $1;
            if($response =~ m/ name="m_ts" value="([^"]*)" /is) {
                my $m_ts = $1;
                if($response =~ m/ name="li" value="([^"]*)" /is) {
                    my $li = $1;
                    if($response =~ m/ name="signup_layout" value="([^"]*)" /is) {
                        my $signup_layout = $1;
                        my $postParam = {
                            'lsd' => $lsd,
                            'charset_test' => $charset_test,
                            'version' => '1',
                            'ajax' => '0',
                            'width' => '0',
                            'pxr' => '0',
                            'gps' => '0',
                            'm_ts' => $m_ts,
                            'li' => $li,
                            'signup_layout' => $signup_layout,
                            'email' => $email,
                            'pass' => $pass,
                            'login' => '登入',
                        };
                        $ua->max_redirect(2);
                        my $response = $ua->post('https://m.facebook.com/login.php?skip_api_login=1&signed_next=1&next=https%3A%2F%2Fm.facebook.com%2Fdialog%2Foauth%3Fredirect_uri%3Dfb412831125460795%253A%252F%252Fauthorize%26display%3Dtouch%26scope%26type%3Duser_agent%26client_id%3D412831125460795%26ret%3Dlogin%26sdk%3Dios&refsrc=https%3A%2F%2Fm.facebook.com%2Flogin.php&refid=9', $postParam);


                        $response = $response->decoded_content();
                        if($response =~ m/ name="fb_dtsg" value="([^"]*)" /is) {
                            my $fb_dtsg = $1;
                            if($response =~ m/ name="charset_test" value="([^"]*)" /is) {
                                $charset_test = $1;
                                $postParam = {
                                    fb_dtsg => $fb_dtsg,
                                    charset_test => $charset_test,
                                    app_id => '412831125460795',
                                    redirect_uri => 'fb412831125460795://authorize/',
                                    display => 'touch',
                                    access_token => '',
                                    sdk => 'ios',
                                    from_post => '1',
                                    login => '',
                                    read => 'public_profile,user_friends',
                                    write => '',
                                    extended => '',
                                    confirm => '',
                                    gdp_version => '3',
                                    seen_scopes => 'public_profile,user_friends',
                                    return_format => 'access_token',
                                    domain => '',
                                    sso_device => 'iphone-safari',
                                    auth_type => '',
                                    auth_nonce => '',
                                    __CONFIRM__ => '完成',
                                };
                                my $response = $ua->post('https://m.facebook.com/dialog/oauth/read', $postParam);
                                my $data =  $response->as_string();
                                if($data =~ m|#access_token=([^&]*)&|si) {
                                    $token = $1;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    if($token eq "") {
        return { user_token => $token, user_id => "" };
    }
    else {
        $response = from_json($ua->get("https://graph.facebook.com/me?access_token=$token")->decoded_content());
        return { user_token => $token, user_id => $response->{id} };
    }
}

1;
