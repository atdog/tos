#!/usr/bin/env perl

use TOSLib::TOS;
use Data::Dumper;
use Twitter;
use Facebook;
use TosUtils;

sub AccountCheck {
    while(my $check = <STDIN>) {
        chomp $check;
        return $check if $check eq '1' or $check eq '2';
        print "[-]\t(1/2): "
    }
}
sub BindCheck {
    while(my $check = <STDIN>) {
        chomp $check;
        return $check if $check eq 'yes' or $check eq 'no';
        print "[-]\t(yes/no): "
    }
}

sub bindProcess {
    my $UNIQUE_KEY = TosCards::getCardLoginKey();

    # Login to ToS
    print "\033[1;30m[+] === TOS Login Data ===\033[0m\n";
    my $tos = TOSLib::TOS->new({
            UNIQUE_KEY => $UNIQUE_KEY,
#             DEVICE_KEY => "", # unique for each device
#             SYS_INFO => "",   # android system info
#             SYSTEM_INFO => "",# android system info
#             UserAgent => "",  # API useragent
        });
    my $loginData = $tos->login();

    print "[+] User Session: $loginData->{'user'}->{'session'}\n";
    print "[+] User ID: $loginData->{'user'}->{'uid'}\n";

    # Card information
    print "\033[1;30m[+] === TOS Cards ===\033[0m\n";
    TosUtils::printCard($loginData);

    # choose account
    print "\033[1;33m[-] 選擇帳號登入 [1: twitter, 2: facebook]: \033[0m";
    $choice = AccountCheck();
    my ($user_id, $user_token, $user_secret);

    if($choice eq "1") {
        # Twitter OAuth
        print "\033[1;30m[+] === Twitter OAuth ===\033[0m\n";
        my ($access_tokens, $account) = Twitter::TwitterOAuth;
        $user_id = $access_tokens->[2];
        $user_token = $access_tokens->[0];
        $user_secret = $access_tokens->[1];
        print "[+] user_token = $user_token\n";
        print "[+] user_secret = $user_secret\n";
        print "[+] user_id = $user_id\n";
        print "[+] username = $access_tokens->[3]\n";
    }
    elsif($choice eq "2") {
        print "\033[1;30m[+] === Facebook OAuth ===\033[0m\n";
        print "\n\033[1;32mEnter your information,\033[0m \n";
        print "\033[1;33mFacebook's account:\033[0m ";
        my $account = <STDIN>;
        print "\033[1;33mFacebook's password: \033[0m";
        my $password = <STDIN>;
        chomp $account;
        chomp $password;
        my $access_tokens = Facebook::loginFacebook($account, $password);

        $user_id = $access_tokens->{user_id};
        $user_token = $access_tokens->{user_token};
        print "\n[+] user_token = $user_token\n";
        print "[+] user_id = $user_id\n";
    }


    # Final Check
    print "\033[1;33m[-] 確認是否綁訂帳號 (yes/no): \033[0m";
    my $check = BindCheck();
    if($check eq "yes") {
        my $ResponseData;
        if($choice eq "1") {
            $ResponseData = $tos->bindTwitter($user_id, $user_token, $user_secret);
        }
        elsif($choice eq "2") {
            $ResponseData = $tos->bindTwitter($user_id, $user_token);
        }
        print "\033[1;31m[!] Error: 帳號已被使用過.\033[0m\n" if $ResponseData->{'respond'} == 7;
        if($ResponseData->{'respond'} == 1) {
            print "\033[1;32m[+] 帳號綁訂成功.\033[0m\n";
            # Update DB
            TosCards::updateDB($UNIQUE_KEY, $account);
        }
    }
    else {
        print "\033[1;32m[-] 綁訂取消.\033[0m\n";
    }
}

sub loginProcess {
    print "\033[1;33m[-] 選擇帳號登入 [1: twitter, 2: facebook]: \033[0m";
    $choice = AccountCheck();
    my ($user_id, $user_token, $user_secret);

    if($choice eq "1") {
        # Twitter OAuth
        print "\033[1;30m[+] === Twitter OAuth ===\033[0m\n";
        my ($access_tokens, $account) = Twitter::TwitterOAuth;
        $user_id = $access_tokens->[2];
        $user_token = $access_tokens->[0];
        $user_secret = $access_tokens->[1];
        print "[+] user_token = $user_token\n";
        print "[+] user_secret = $user_secret\n";
        print "[+] user_id = $user_id\n";
        print "[+] username = $access_tokens->[3]\n";
        my $tos = TOSLib::TOS->new({
                UNIQUE_KEY => $UNIQUE_KEY,
#             DEVICE_KEY => "", # unique for each device
#             SYS_INFO => "",   # android system info
#             SYSTEM_INFO => "",# android system info
#             UserAgent => "",  # API useragent
            });
        my $loginData = $tos->loginTwitter($user_id, $user_token, $user_secret);

        TosUtils::printCard($loginData);
    }
    elsif($choice eq "2") {
        print "\033[1;30m[+] === Facebook OAuth ===\033[0m\n";
        print "\n\033[1;32mEnter your information,\033[0m \n";
        print "\033[1;33mFacebook's account:\033[0m ";
        my $account = <STDIN>;
        print "\033[1;33mFacebook's password: \033[0m";
        my $password = <STDIN>;
        chomp $account;
        chomp $password;
        my $access_tokens = Facebook::loginFacebook($account, $password);

        $user_id = $access_tokens->{user_id};
        $user_token = $access_tokens->{user_token};
        print "\n[+] user_token = $user_token\n";
        print "[+] user_id = $user_id\n";
        my $tos = TOSLib::TOS->new({
                UNIQUE_KEY => $UNIQUE_KEY,
#             DEVICE_KEY => "", # unique for each device
#             SYS_INFO => "",   # android system info
#             SYSTEM_INFO => "",# android system info
#             UserAgent => "",  # API useragent
            });
        my $loginData = $tos->loginFacebook($user_id, $user_token);

        TosUtils::printCard($loginData);
    }
}

sub main {
    print "\033[1;33m[-] 選擇綁定或登入 [1: 綁定, 2: 登入]: \033[0m";
    my $choice = AccountCheck();
    if($choice eq "1") {
        bindProcess;
    }
    elsif($choice eq "2") {
        loginProcess;
    }



#     my $loginData = $tos->loginFacebook($access_tokens->{user_id}, $access_tokens->{user_token});

#     TosUtils::printCard($loginData);

}

main;
