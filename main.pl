#!/usr/bin/env perl

use Twitter;
use TosUtils;
use TosCards;
use JSON; # imports encode_json, decode_json, to_json and from_json.
use Data::Dumper;
use  Tie::StoredOrderHash qw/ ordered is_ordered /;
use strict;

# initialize variable
my $TosAPIhost = "http://zh.towerofsaviors.com";
# my $UNIQUE_KEY = "c1aa3237fb08ce29593d8b809b32d2af";
my $DEVICE_KEY = TosUtils::getDeviceKey();
my $TIMESTAMP = time;
my $MACaddress = sprintf("%.2X:",rand(255))for(1..5).sprintf("%.2X",rand(255));
my $postParam = {
    systemInfo => qq({"appVersion":"3.27","deviceModel":"HTC+HTC+Explorer+A310e","deviceType":"Handheld","deviceUniqueIdentifier":"$DEVICE_KEY","operatingSystem":"Android+OS+2.3.5+/+API-10+(GRJ90/179613.1)","systemVersion":"2.3.5","processorType":"ARMv7+VFPv3+NEON","processorCount":"1","systemMemorySize":"408","graphicsMemorySize":"104","graphicsDeviceName":"Adreno+(TM)+200","graphicsDeviceVendor":"Qualcomm","graphicsDeviceVersion":"OpenGL+ES-CM+1.1","emua":"FALSE","emub":"FALSE","npotSupport":"Full","supportsAccelerometer":"True","supportsGyroscope":"False","supportsLocationService":"True","supportsVibration":"True","maxTextureSize":"4096","screenWidth":"320","screenHeight":"480","screenDPI":"159.8968","IDFA":"","IDFV":"","MAC":"$MACaddress","networkType":"WIFI"})
};

sub LoginTOS {
    my $UNIQUE_KEY = shift or die $!;
    my $getParam = ordered [
        type => "device",
        uniqueKey => $UNIQUE_KEY,
        deviceKey => $DEVICE_KEY,
        sysinfo => "Android+OS+2.3.5+/+API-10+(GRJ90/179613.1)|ARMv7+VFPv3+NEON|1|408|104|Adreno+(TM)+200|FALSE|OpenGL+ES-CM+1.1|Full|FALSE|4096|3.27|||$MACaddress"
    ];
    my $url = TosUtils::getValidURL("/api/user/login", $getParam);
    print $url."\n";

    my $ua = LWP::UserAgent->new(); 
    $ua->agent('Dalvik/1.6.0 (Linux; U; Android 4.1.2; GT-I9300 Build/JZO54K)');
    my $response = $ua->post($TosAPIhost.$url, $postParam);
    my $json = from_json($response->decoded_content());
    print $response->decoded_content() and exit if $json->{'respond'} != 1;
    return $json;
}

sub RebindTOS {
    my $LoginData = shift or die $!;
    my $access_tokens = shift or die $!;
    my $getParam = {
        type => "twitter",
        uid => $LoginData->{'user'}->{'uid'},
        session => $LoginData->{'user'}->{'session'},
        user_id => $access_tokens->[2],
        user_token => $access_tokens->[0],
        user_secret => $access_tokens->[1]
    };
    my $url = TosUtils::getValidURL("/api/user/rebind", $getParam);
#     print $url."\n";

    my $ua = LWP::UserAgent->new(); 
    $ua->agent('Dalvik/1.6.0 (Linux; U; Android 4.1.2; GT-I9300 Build/JZO54K)');
    my $response = $ua->post($TosAPIhost.$url, $postParam);
    my $json = from_json($response->decoded_content());
    print $response->decoded_content() if $json->{'respond'} != 1;
#     print $response->decoded_content();
    return $json;
}

sub main {
    my $UNIQUE_KEY = TosCards::getCardLoginKey();

    # Login to ToS
    print "\033[1;30m[+] === TOS Login Data ===\033[0m\n";
    my $LoginData = LoginTOS($UNIQUE_KEY);
    print "[+] User Session: $LoginData->{'user'}->{'session'}\n";
    print "[+] User ID: $LoginData->{'user'}->{'uid'}\n";
    # Card information
    print "\033[1;30m[+] === TOS Cards ===\033[0m\n";
    TosUtils::printCard($LoginData);

    # Twitter OAuth
    print "\033[1;30m[+] === Twitter OAuth ===\033[0m\n";
    my ($access_tokens, $account) = Twitter::TwitterOAuth;
    print "[+] user_token = $access_tokens->[0]\n";
    print "[+] user_secret = $access_tokens->[1]\n";
    print "[+] userid = $access_tokens->[2]\n";
    print "[+] username = $access_tokens->[3]\n";

    # Final Check
    print "\033[1;33m[-] 確認是否綁訂帳號 (yes/no): \033[0m";
    my $check = TosUtils::BindCheck();
    if($check eq "yes") {
        my $ResponseData = RebindTOS($LoginData, $access_tokens);
        print "\033[1;31m[!] Error: 帳號已被使用過.\033[0m\n" if $ResponseData->{'respond'} == 7;
        if($ResponseData->{'respond'} == 1) {
            print "\033[1;32m[+] 帳號綁訂成功.\033[0m\n";
            # Update DB
            TosCards::updateDB($UNIQUE_KEY, $account);
        }
    }
    else {
        print "\033[1;32m[-] 綁訂取消.\033[0m\n";
    }

}

main;
