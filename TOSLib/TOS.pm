package TOSLib::TOS;

use Digest::MD5 qw/md5_hex/;
use Tie::StoredOrderHash qw/ordered/;
use URI::Escape;
use LWP::UserAgent;
use JSON;

my $TosAPIhost = "http://zh.towerofsaviors.com";

sub new {
    my ($class, $args) = @_;
    bless {%$args}, $class;
}

sub API {
    my ($self, $path, $getParam) = @_;

    my $SYSTEM_INFO = getSystemInfo($self);
    my $postParam = {
        systemInfo => $SYSTEM_INFO
    };

    my $url = getValidURL($path, $getParam);
    my $ua = LWP::UserAgent->new(); 
    $ua->agent(getUserAgent($self));
    my $response = $ua->post($TosAPIhost.$url, $postParam);
    my $json = from_json($response->decoded_content());
    # save session
    $self->{user} = $json->{user};
    return $json;
}

sub login {
    my ($self, $type) = @_;
    my $UNIQUE_KEY = $self->{UNIQUE_KEY};
    my $DEVICE_KEY = getDeviceKey($self);
    my $SYS_INFO = getSysInfo($self);
    $type = "device" if not $type; 

    my $getParam = ordered [
        type => $type,
        uniqueKey => $UNIQUE_KEY,
        deviceKey => $DEVICE_KEY,
        sysinfo => $SYS_INFO
    ];
    return $self->API("/api/user/login", $getParam);
}

sub bindTwitter {
    my ($self, $user_id, $user_token, $user_secret) = @_;
    print "Please login first." and exit if not $self->{user};
    my $getParam = ordered [
        type => "twitter",
        uid => $self->{user}->{uid},
        session => $self->{user}->{session},
        user_id => $user_id,
        user_token => $user_token,
        user_secret => $user_secret
    ];

    return $self->API("/api/user/rebind", $getParam);
}
sub bindFacebook {
    my ($self, $user_id, $user_token) = @_;
    print "Please login first." and exit if not $self->{user};
    my $getParam = ordered [
        type => "facebook",
        uid => $self->{user}->{uid},
        session => $self->{user}->{session},
        user_id => $user_id,
        user_token => $user_token
    ];

    return $self->API("/api/user/rebind", $getParam);
}

sub loginTwitter {
    my ($self, $user_id, $user_token, $user_secret) = @_;
    my $getParam = ordered [
        type => "twitter",
        user_id => $user_id,
        user_token => $user_token,
        user_secret => $user_secret
    ];

    my $loginData = $self->API("/api/user/bind", $getParam);

    if( $loginData->{respond} == 1 ) {
        $self->{UNIQUE_KEY} = $loginData->{data}->{uniqueKey};

        return $self->login("twitter");
    }
    return { respond => "-1" };
}

sub loginFacebook {
    my ($self, $user_id, $user_token) = @_;

    my $getParam = ordered [
        type => "facebook",
        user_id => $user_id,
        user_token => $user_token,
    ];
    my $loginData = $self->API("/api/user/bind", $getParam);

    if( $loginData->{respond} == 1 ) {
        $self->{UNIQUE_KEY} = $loginData->{data}->{uniqueKey};

        return $self->login("facebook");
    }
    return { respond => "-1" };

}

sub getUserAgent {
    my $self = shift or die $!;
    return $self->{UserAgent} if $self->{UserAgent};
    return 'Dalvik/1.6.0 (Linux; U; Android 4.1.2; GT-I9300 Build/JZO54K)';
}

sub getDeviceKey {
    my $self = shift or die $!;
    return $self->{DEVICE_KEY} if $self->{DEVICE_KEY};
    return md5_hex(time);
}

sub getSystemInfo {
    my $self = shift or die $!;
    return $self->{SYSTEM_INFO} if $self->{SYSTEM_INFO};
    return qq({"appVersion":"3.27","deviceModel":"HTC+HTC+Explorer+A310e","deviceType":"Handheld","deviceUniqueIdentifier":"$DEVICE_KEY","operatingSystem":"Android+OS+2.3.5+/+API-10+(GRJ90/179613.1)","systemVersion":"2.3.5","processorType":"ARMv7+VFPv3+NEON","processorCount":"1","systemMemorySize":"408","graphicsMemorySize":"104","graphicsDeviceName":"Adreno+(TM)+200","graphicsDeviceVendor":"Qualcomm","graphicsDeviceVersion":"OpenGL+ES-CM+1.1","emua":"FALSE","emub":"FALSE","npotSupport":"Full","supportsAccelerometer":"True","supportsGyroscope":"False","supportsLocationService":"True","supportsVibration":"True","maxTextureSize":"4096","screenWidth":"320","screenHeight":"480","screenDPI":"159.8968","IDFA":"","IDFV":"","MAC":"$MACaddress","networkType":"WIFI"});
}

sub getSysInfo {
    my $self = shift or die $!;
    return $self->{SYS_INFO} if $self->{SYS_INFO};
    my $MACaddress = "";
    for(1..5) {
        $MACaddress .= sprintf("%.2X:",rand(255));
    }
    $MACaddress .= sprintf("%.2X",rand(255));
    return "Android+OS+2.3.5+/+API-10+(GRJ90/179613.1)|ARMv7+VFPv3+NEON|1|408|104|Adreno+(TM)+200|FALSE|OpenGL+ES-CM+1.1|Full|FALSE|4096|3.27|||$MACaddress"
}

sub getValidURL {
    my $path = shift or die $!;
    my $getParam = shift or die $!;

    $getParam->{'language'} = "zh_TW",
    $getParam->{'platform'} = "android",
    $getParam->{'version'} = "3.27",
    $getParam->{'timestamp'} = time;
    $getParam->{'timezone'} = 8;
    $getParam->{'nData'} = md5_hex(substr "YmZhYTRkNzIwM2VkODhhZTZiZTg4MDU2NWFlYjkxMDU=", int(rand(16)), int(rand(12) + 4));

    my $url = $path."?".escape_hash($getParam);
    my $hash = getHash($url);
    return $url."&hash=$hash";
}

sub escape_hash {
    my $hash = shift or die $!;
    my @pairs;
    for my $key (keys %$hash) {
        push @pairs, join "=", map { uri_escape($_) } $key, $hash->{$key};
    }
    return join "&", @pairs;
}

sub getHash {
    my $request = shift or die $!;

    my $str = "YmZhYTRkNzIwM2VkODhhZTZiZTg4MDU2NWFlYjkxMDU=";
    my $result = md5_hex(substr(md5_hex($request), 4, 4).$str);

    return $result;
}
1;
