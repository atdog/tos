package CardsDB;
use DBI;
use strict;

sub new {
    my ($class, %args) = @_;
    my $dsn = 'dbi:SQLite:dbname=cards.db'; 
    $args{'dbh'} = DBI->connect($dsn, "", "", { RaiseError => 1 }) or die $DBI::errstr;
    return bless { %args }, $class;
}

sub updateDB {
    my ($self, $key, $account) = @_;
    my $sth = $self->{'dbh'}->prepare("update cards set isBind = 'true', account = '$account' where key = '$key';");
    $sth->execute();
}

sub getAllCardsCount {
    my ($self) = @_;
    my $sth = $self->{'dbh'}->prepare("select count(*) from cards where isBind = 'false';");
    $sth->execute();

    my $ver = $sth->fetch();
    $sth->finish();
    return $ver->[0];
}

sub getAllKindOfCardsCount {
    my ($self) = @_;
    my $sth = $self->{'dbh'}->prepare("select count(*) from (select id from cards where isBind = 'false' group by id);");
    $sth->execute();

    my $ver = $sth->fetch();
    $sth->finish();
    return $ver->[0];
}

sub getEachKindOfCardsCount {
    my ($self) = @_;
    my $sth = $self->{'dbh'}->prepare("select id, count(*) from cards where isBind = 'false' group by id;");
    $sth->execute();

    my $ver = $sth->fetchall_arrayref();
    $sth->finish();
    return $ver;
}

sub getCardLoginKey {
    my ($self, $CardID) = @_;
    my $sth = $self->{'dbh'}->prepare("select key from cards where id = $CardID and isBind = 'false' limit 1;");
    $sth->execute();

    my $ver = $sth->fetch();
    $sth->finish();
    return $ver->[0];
}

sub DESTROY {
    my $self = shift;
    $self->{'dbh'}->disconnect();
}

1;
