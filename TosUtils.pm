package TosUtils;

use strict;
use Digest::MD5 qw(md5 md5_base64 md5_hex);
use TosHash;
use TosCards;
use URI::Escape;

sub getDeviceKey {
    my $md5 = md5_hex(time);
    return $md5;
}

sub getValidURL {
    my $path = shift or die $!;
    my $getParam = shift or die $!;

    $getParam->{'language'} = "zh_TW",
    $getParam->{'platform'} = "android",
    $getParam->{'version'} = "3.27",
    $getParam->{'timestamp'} = time;
    $getParam->{'timezone'} = 8;
    $getParam->{'nData'} = md5_hex(substr "YmZhYTRkNzIwM2VkODhhZTZiZTg4MDU2NWFlYjkxMDU=", int(rand(16)), int(rand(12) + 4));

    my $url = $path."?".escape_hash($getParam);
    my $hash = TosHash::getHash($url);
    return $url."&hash=$hash";
}

sub escape_hash {
    my $hash = shift or die $!;
    my @pairs;
    for my $key (keys %$hash) {
        push @pairs, join "=", map { uri_escape($_) } $key, $hash->{$key};
    }
    return join "&", @pairs;
}

sub printCard {
    my $LoginData = shift or die $!;

    print "[+] All Cards\n";
    my $cards = $LoginData->{'cards'};
    for my $card (@$cards) {
        # 1|1|0|1|1|1372248357|0
        my @c = split '\|', $card;
        my $cardName = TosCards::getCardName($c[1]);
        print "[+] \tCard$c[0] ID: $c[1], Name: $cardName, LV: $c[3], SLV: $c[4]\n";
    }
}

sub BindCheck {
    while(my $check = <STDIN>) {
        chomp $check;
        return $check if $check eq 'yes' or $check eq 'no';
        print "[-]\t(yes/no): "
    }
}


1
